//#include <stdio.h>

#include "dirent_c.h"
/***************************************************************************/
static int save_data( char * fname,  char * data, int dataLen )
{

    FILE *write_ptr;

    write_ptr = fopen(fname,"wb");

    fwrite(data,dataLen,1,write_ptr);

    fclose(write_ptr);

    return 0;
}

/***************************************************************************/
static int load_data( char * fname,  char * data, int maxDataLen )
{

    FILE *write_ptr;

    write_ptr = fopen(fname,"rb");

    int readed=fread(data,1,maxDataLen,write_ptr);

    fclose(write_ptr);

    return readed;
}
/***************************************************************************/
static int cmd_Files( void )
{
    DIR * theDir;

    theDir = opendir( "." );
    if( !theDir ) return -2;

    struct dirent *theDirEnt = readdir( theDir );
    while( theDirEnt ) {
        if ( *theDirEnt->d_name != '.')
            printf( "  %s\n", theDirEnt->d_name );
	theDirEnt = readdir( theDir );
    }
    closedir( theDir );

    return 0;
}

/***********************************************************/
static void os_outchar(unsigned char c)
{
    putchar(c);
}
/***********************************************************/
static unsigned char os_getchar()
{
    return getchar();
}
/***********************************************************/
