'------------------------------------------------------------------------
'' getchar
'------------------------------------------------------------------------
Function getchar() as ubyte

    Return GetKey()

end Function 
'------------------------------------------------------------------------
'' outchar
'------------------------------------------------------------------------
Sub outchar(char As ubyte)

     print chr(char);

end Sub 
'------------------------------------------------------------------------
'------------------------------------------------------------------------
'' main
'------------------------------------------------------------------------
Function fbMain(byref URL as string) as uinteger


      Dim hndl As Any Ptr

      hndl=DyLibLoad("TinyBasicPlus.dll")

      if hndl=0 then
          print "TinyBasicPlus.dll not found"
          return 1
      end if

      Dim tb_setup As Sub ()
      Dim tb_loop As Sub ()
      Dim tb_setupIO As Sub( Soutchar As Sub( char As ubyte), Sgetchar As Function () As ubyte )

      tb_setup = DyLibSymbol( hndl, "setup" )
      tb_loop = DyLibSymbol( hndl, "loop" )
      tb_setupIO = DyLibSymbol( hndl, "setupIO" )

      tb_setupIO(@outchar,@getchar)
      tb_setup()
      tb_loop()
      
      return 0

end Function 
'------------------------------------------------------------------------
fbMain(Command(1))
end
